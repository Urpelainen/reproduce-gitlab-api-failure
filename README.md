# reproduce-gitlab-api-failure

Reproduce Gitlab Rest Api v4 failure to show commit's merge request when merge request has no commit on top of its root commit.

This repository has a merged merge request with a single commit, !1. After !1 was rebased and merged, another branch and merge request, !2, were opened on the same commit. This procedure of first merging a merge request, then opening a new one on the same commit leads to situation where the merge request is not visible in Rest Api v4 call commits/&lt;sha&gt;/merge_requests.

To reproduce:
1. Create a gitlab.com user and api token
2. Fetch merge requests of the relevant commit:
      curl --header "PRIVATE-TOKEN: &lt;your token here&gt;" "https://gitlab.com/api/v4/projects/22779992/repository/commits/c4b05f424dd4caf564eece73cbec71daced4ce23/merge_requests"
3. Observe that the merge request !1 was included
4. But that !2 was not (WRONG!)
5. Check from GitLab web ui that actually merge request !2 points to commit c4b05f42

To reproduce in another repository:
1. Initalize a new repository
2. Configure to use only fast-forward merges
1. Create a branch, a commit and a merge request on top of _master.
2. Merge the merge request to _master_
3. Create a new merge request on top of _master_
4. List merge requests of current _master_ commit using the v4 Api
5. Observe that the new merge request is not listed